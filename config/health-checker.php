<?php

use Adranetwork\HealthChecker\CheckType;

return [
    'cache_results' => [
        'enabled' => true,
        'ttl' => 3600, // in seconds
        'key' => 'health_checker.'.env('GIT_SENTRY_RELEASE', '').'.results'
    ],
    'current_release_tag' => env('GIT_SENTRY_RELEASE', null),

    // Name should be identifiable
    // Check CheckType enum for all the enum types
    // You can have multiple of the storage type

    'checks' => [
        [
            'name' => 'Database Connection',
            'type' => CheckType::Database,
            'enabled' => true,
        ],
        [
            'name' => 'Redis Connection',
            'type' => CheckType::Redis,
            'enabled' => true,
        ],
        [
            'name' => 'Cache Connection',
            'type' => CheckType::Cache,
            'enabled' => true,
        ],
        [
            'name' => 'S3 Storage',
            'type' => CheckType::Storage,
            'enabled' => true,
            'options' => [
                'disk' => 's3', // the actual filesystem disk
            ],
        ],

    ],
];
