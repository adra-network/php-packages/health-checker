<?php

namespace Adranetwork\HealthChecker\Contracts;

use Illuminate\Support\Collection;

interface ConfigurationService
{
    public function enabledChecks(): Collection;
}
