<?php

namespace Adranetwork\HealthChecker\Contracts;

use Illuminate\Support\Collection;

interface HealthCheckProvider
{
    public function register(): void;

    public function verifyAllChecks(): bool;

    public function results(): Collection;
}
