<?php

namespace Adranetwork\HealthChecker;

enum CheckType
{
    case Database;
    case Redis;
    case Cache;
    case Storage;
    case Ping;

}
