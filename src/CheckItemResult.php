<?php

namespace Adranetwork\HealthChecker;

class CheckItemResult
{
    public function __construct(
        public string $checkName,
        public string $status,
        public array $additional = []
    ) {
    }
}
