<?php

namespace Adranetwork\HealthChecker\HealthProviders;

use Adranetwork\HealthChecker\CheckItem;
use Adranetwork\HealthChecker\CheckItemResult;
use Adranetwork\HealthChecker\Checks\NullCheck;
use Adranetwork\HealthChecker\Checks\S3StorageCheck;
use Adranetwork\HealthChecker\CheckType;
use Adranetwork\HealthChecker\Contracts\ConfigurationService;
use Adranetwork\HealthChecker\Contracts\HealthCheckProvider;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Artisan;
use Spatie\Health\Checks\Checks\CacheCheck;
use Spatie\Health\Checks\Checks\DatabaseCheck;
use Spatie\Health\Checks\Checks\RedisCheck;
use Spatie\Health\Commands\RunHealthChecksCommand;
use Spatie\Health\Facades\Health;
use Spatie\Health\ResultStores\ResultStore;
use Spatie\Health\ResultStores\StoredCheckResults\StoredCheckResult;

class SpatieHealthCheckProvider implements HealthCheckProvider
{
    public function __construct(
        private readonly ResultStore $resultStore,
        protected readonly ConfigurationService $configurationService
    ) {
        $this->register();
    }

    private function mapChecks(): array
    {
        return $this->configurationService->enabledChecks()
            ->map(fn (CheckItem $check) => match ($check->type) {
                CheckType::Database => DatabaseCheck::new()->name($check->name),
                CheckType::Redis => RedisCheck::new()->name($check->name),
                CheckType::Cache => CacheCheck::new()->name($check->name),
                CheckType::Storage => S3StorageCheck::new()->name($check->name)->disk($check->options['disk'] ?? ''),
                default => NullCheck::new()->name($check->name)
            })->toArray();
    }

    public function register(): void
    {
        Health::checks($this->mapChecks());
    }

    public function verifyAllChecks(): bool
    {
        Artisan::call(RunHealthChecksCommand::class);

        return $this->resultStore->latestResults()->allChecksOk();
    }

    public function results(): Collection
    {
        return $this->resultStore->latestResults()->storedCheckResults
            ->map(function (StoredCheckResult $check) {
                return new CheckItemResult(
                    checkName: $check->name,
                    status: $check->status,
                    additional: $check->toArray()
                );
            });

    }
}
