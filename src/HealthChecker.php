<?php

namespace Adranetwork\HealthChecker;

use Adranetwork\HealthChecker\Contracts\HealthCheckProvider;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

class HealthChecker implements HealthCheckProvider
{
    public function __construct(
        public readonly HealthCheckProvider $healthCheckProvider,
    ) {
    }

    public function register(): void
    {
        $this->healthCheckProvider->register();
    }

    public function verifyAllChecks(): bool
    {
        if (config('health-checker.cache_results.enabled')) {
            return Cache::remember(
                key: config('health-checker.cache_results.key'),
                ttl: config('health-checker.cache_results.ttl'),
                callback: fn () => $this->healthCheckProvider->verifyAllChecks()
            );
        }
        return $this->healthCheckProvider->verifyAllChecks();

    }

    public function results(): Collection
    {
        return $this->healthCheckProvider->results();
    }
}
