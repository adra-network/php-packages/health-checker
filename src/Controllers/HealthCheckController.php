<?php

namespace Adranetwork\HealthChecker\Controllers;

use Adranetwork\HealthChecker\Contracts\HealthCheckProvider;
use Adranetwork\HealthChecker\Exceptions\HealthCheckFailedException;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;
use Spatie\Health\ResultStores\ResultStore;

class HealthCheckController extends Controller
{
    public function __construct(
        protected HealthCheckProvider $healthCheckProvider,
        protected ResultStore $store
    ) {
    }

    public function __invoke(): JsonResponse
    {
        if ($this->healthCheckProvider->verifyAllChecks()) {
            return response()->json([
                'health_checks' => 'passed',
                'release' => config('health-checker.current_release_tag'),
            ]);
        }
        Log::critical(
            message: 'Failed health check',
            context: $this->healthCheckProvider->results()->toArray()
        );
        throw HealthCheckFailedException::withResults($this->healthCheckProvider->results()->toArray());
    }
}
