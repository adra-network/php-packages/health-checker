<?php

namespace Adranetwork\HealthChecker\Services;

use Adranetwork\HealthChecker\CheckItem;
use Adranetwork\HealthChecker\Contracts\ConfigurationService;
use Adranetwork\HealthChecker\Exceptions\InvalidCheckException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

class DefaultConfigurationService implements ConfigurationService
{
    /** @return Collection<CheckItem> */
    public function enabledChecks(): Collection
    {
        return Cache::remember('enabled_health_checks', 3600, function () {
            return collect(config('health-checker.checks'))
                ->where('enabled', '=', true)
                ->map(function ($configCheck) {
                    try {
                        return CheckItem::fromConfig($configCheck);
                    } catch (\Exception $e) {
                        throw new InvalidCheckException();
                    }
                });
        });

    }
}
