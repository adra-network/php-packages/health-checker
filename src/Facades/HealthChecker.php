<?php

namespace Adranetwork\HealthChecker\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Adranetwork\HealthChecker\HealthChecker
 */
class HealthChecker extends Facade
{
    protected static function getFacadeAccessor()
    {
        return \Adranetwork\HealthChecker\HealthChecker::class;
    }
}
