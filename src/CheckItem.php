<?php

namespace Adranetwork\HealthChecker;

use Illuminate\Support\Str;

class CheckItem
{
    public function __construct(
        public readonly string $name,
        public readonly CheckType $type,
        public readonly array $options = [],
    ) {
    }

    public static function fromConfig(array $configItem = []): self
    {
        $baseName = $configItem['name'] ?? $configItem['type']->name;

        return new self(
            name: sprintf('%s_%s', $baseName, Str::of(Str::random(4))->lower()->toString()),
            type: $configItem['type'],
            options: $configItem['options'] ?? []
        );
    }
}
