<?php

namespace Adranetwork\HealthChecker\Commands;

use Illuminate\Console\Command;

class HealthCheckerCommand extends Command
{
    public $signature = 'health-checker';

    public $description = 'My command';

    public function handle(): int
    {
        $this->comment('All done');

        return self::SUCCESS;
    }
}
