<?php

namespace Adranetwork\HealthChecker\Checks;

use Spatie\Health\Checks\Check;
use Spatie\Health\Checks\Result;

class NullCheck extends Check
{
    public function run(): Result
    {
        return Result::make()->ok();
    }
}
