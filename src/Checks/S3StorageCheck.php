<?php

namespace Adranetwork\HealthChecker\Checks;

use Exception;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Spatie\Health\Checks\Check;
use Spatie\Health\Checks\Result;

final class S3StorageCheck extends Check
{
    public string $disk = 's3';

    protected string $file = 'health-check.txt';

    protected string $content;

    public static function new(): static
    {

        $instance = new self();

        $instance->content('');
        $instance->file(Str::of(Str::random(32))->slug()->append('.txt')->toString());

        $instance->disk('s3');

        $instance->everyMinute();

        return $instance;

    }

    public function content(string $content): self
    {
        $this->content = $content;

        return $this;
    }
    public function file(string $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function disk(string $disk = 's3'): self
    {
        $this->disk = $disk;

        return $this;
    }

    public function run(): Result
    {
        try {
            $result = Result::make();


            Storage::disk($this->disk)->put(
                $this->file,
                $this->content
            );
            $fileExists = Storage::disk($this->disk)
                ->exists($this->file);

            Storage::disk($this->disk)->delete($this->file);

            if (!$fileExists ) {
                $result->failed('Amazon S3 connection is failing');
                return $result;
            }
            return $result->ok();
        } catch (Exception $exception) {
            report($exception);
            $result = Result::make();

            return $result->failed($exception->getMessage());
        }
    }
}
