<?php

namespace Adranetwork\HealthChecker;

use Adranetwork\HealthChecker\Contracts\ConfigurationService;
use Adranetwork\HealthChecker\Contracts\HealthCheckProvider;
use Adranetwork\HealthChecker\HealthProviders\SpatieHealthCheckProvider;
use Adranetwork\HealthChecker\Services\DefaultConfigurationService;
use Illuminate\Support\Facades\Config;
use Spatie\Health\HealthServiceProvider;
use Spatie\Health\ResultStores\CacheHealthResultStore;
use Spatie\Health\ResultStores\InMemoryHealthResultStore;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;

class HealthCheckerServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {

        $package
            ->name('health-checker')
            ->hasRoute('routes')
            ->hasConfigFile();
    }

    public function packageRegistered(): void
    {
        $this->app->register(HealthServiceProvider::class);

        Config::set('health.result_stores', [
            InMemoryHealthResultStore::class,
        ]);

        Config::set('health.notifications.enabled', false);

        $this->app->singleton(ConfigurationService::class, fn () => new DefaultConfigurationService());

        $this->app->singleton(HealthCheckProvider::class,
            fn ($app) => new HealthChecker($app->make(SpatieHealthCheckProvider::class))
        );
    }

    public function bootingPackage(): void
    {

    }
}
