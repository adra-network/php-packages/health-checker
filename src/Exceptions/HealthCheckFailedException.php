<?php

namespace Adranetwork\HealthChecker\Exceptions;

use RuntimeException;
use Throwable;

class HealthCheckFailedException extends RuntimeException
{
    public function __construct(string $message = '', int $code = 0, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    protected array $results = [];

    public static function withResults(array $results = []): HealthCheckFailedException
    {
        $instance = new self();
        $instance->results = $results;

        return $instance;
    }

    public function context()
    {
        return [
            'check_results' => $this->results,
        ];
    }

    public function render()
    {
        return response()->json([
            'health_checks' => 'failed',
            'release' => config('health-checker.current_release_tag'),
        ], 500);
    }
}
