<?php

use Adranetwork\HealthChecker\Controllers\HealthCheckController;
use Illuminate\Support\Facades\Route;

Route::get('/health', HealthCheckController::class)->name('health-checker.health');
