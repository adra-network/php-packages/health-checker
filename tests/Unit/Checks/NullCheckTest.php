<?php

namespace Adranetwork\HealthChecker\Tests\Unit\Checks;

use Adranetwork\HealthChecker\Checks\NullCheck;
use Adranetwork\HealthChecker\Tests\TestCase;
use Spatie\Health\Checks\Result;
use Spatie\Health\Enums\Status;

class NullCheckTest extends TestCase
{
    /** @test **/
    public function it_returns_ok()
    {
        $result = NullCheck::new()
            ->run();
        $this->assertInstanceOf(Result::class, $result);
        $this->assertEquals($result->status, Status::ok());
    }
}
