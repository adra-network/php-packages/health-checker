<?php

namespace Adranetwork\HealthChecker\Tests\Unit\Checks;

use Adranetwork\HealthChecker\Checks\S3StorageCheck;
use Adranetwork\HealthChecker\Tests\TestCase;
use Illuminate\Support\Facades\Storage;
use Spatie\Health\Checks\Result;
use Spatie\Health\Enums\Status;

class StorageCheckTest extends TestCase
{
    /** @test **/
    public function it_uses_s3_driver_by_default()
    {
        $check = S3StorageCheck::new();
        $this->assertEquals('s3', $check->disk);

    }

    /** @test **/
    public function it_returns_ok()
    {
        //        self::markTestSkipped();

        Storage::fake('s3');
        $result = S3StorageCheck::new()->run();
        $this->assertInstanceOf(Result::class, $result);
        $this->assertEquals($result->status, Status::ok());
    }

    /** @test **/
    public function it_returns_failed()
    {
        self::markTestSkipped();

        // here we dont fake the Storage so it'll throw an error
        $result = S3StorageCheck::new()->run();
        $this->assertInstanceOf(Result::class, $result);
        $this->assertEquals($result->status, Status::failed());
    }
}
