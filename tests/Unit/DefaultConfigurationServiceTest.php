<?php

use Adranetwork\HealthChecker\CheckType;
use Adranetwork\HealthChecker\Contracts\ConfigurationService;
use Adranetwork\HealthChecker\Exceptions\InvalidCheckException;
use Adranetwork\HealthChecker\Services\DefaultConfigurationService;

test('its_the default_resolution_of_service_container', function () {
    $service = app()->make(ConfigurationService::class);
    expect($service)
        ->toBeInstanceOf(DefaultConfigurationService::class);
});

test('it_returns_enabled_checks', function () {

    config([
        'health-checker.checks' => [
            [
                'enabled' => true,
                'type' => CheckType::Database,
            ],
            [
                'enabled' => false,
                'type' => CheckType::Database,
            ],
        ],
    ]);

    $service = new DefaultConfigurationService();

    expect($service->enabledChecks())
        ->toHaveCount(1);
});

test('it_validates_check_types_are_valid', function () {

    config([
        'health-checker.checks' => [
            [
                'enabled' => true,
                'type' => 'invalid-type',
            ],
            [
                'enabled' => false,
                'type' => CheckType::Database,
            ],
        ],
    ]);
    $service = new DefaultConfigurationService();
    $service->enabledChecks();
})->throws(InvalidCheckException::class);
