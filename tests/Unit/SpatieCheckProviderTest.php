<?php

use Adranetwork\HealthChecker\CheckType;
use Adranetwork\HealthChecker\HealthProviders\SpatieHealthCheckProvider;
use Spatie\Health\Facades\Health;

test('it_verifies_checks', function () {

    config([
        'health-checker.checks' => [
            [
                'enabled' => true,
                'type' => CheckType::Database,
            ],
        ],
    ]);
    $checker = app()->make(SpatieHealthCheckProvider::class);

    $result = $checker->verifyAllChecks();
    expect($result)
        ->toBeTrue();

});

test('it_doesnt_register_disabled_checks', function () {
    config([
        'health-checker.checks' => [
            [
                'enabled' => false,
                'type' => CheckType::Database,
            ],
        ],
    ]);

    app()->make(SpatieHealthCheckProvider::class);
    expect(Health::registeredChecks())
        ->toHaveCount(0);
});

test('it_registers_checks_of_same_type', function () {
    config([
        'health-checker.checks' => [
            [
                'enabled' => true,
                'type' => CheckType::Database,
            ],
            [
                'enabled' => true,
                'type' => CheckType::Database,
            ],
        ],
    ]);

    app()->make(SpatieHealthCheckProvider::class);
    expect(Health::registeredChecks())
        ->toHaveCount(2);

});

test('it_maps_all_default_config_correctly', function () {

    config([
        'health-checker.checks' => [
            [
                'enabled' => true,
                'type' => CheckType::Storage,
                'options' => [
                    'disk' => $disk = 's3_test',
                ],
            ],
        ],
    ]); //    dd($expectedCount);
    app()->make(SpatieHealthCheckProvider::class);

    expect(Health::registeredChecks())
        ->toHaveCount(1);

});
test('it_maps_storage_with_correct_options', function () {

    config([
        'health-checker.checks' => [
            [
                'enabled' => true,
                'type' => CheckType::Storage,
                'options' => [
                    'disk' => $disk = 's3_test',
                ],
            ],
        ],
    ]);
    app()->make(SpatieHealthCheckProvider::class);

    expect(Health::registeredChecks())
        ->toHaveCount(1);
    expect(Health::registeredChecks()->first()->disk)
        ->toEqual($disk);

});
