<?php

use Adranetwork\HealthChecker\CheckType;

it('it_appends_random_character_to_name_after_type', function () {
    $checkData = [
        'enabled' => true,
        'type' => $type = CheckType::Database,
    ];
    $item = \Adranetwork\HealthChecker\CheckItem::fromConfig($checkData);
    expect($item->name)
        ->toContain($type->name.'_');

    expect(strlen($item->name))
        // 8 being the type name and 5 being the _ + random 4 char
        ->toEqual(8 + 5);
});
