<?php

use Adranetwork\HealthChecker\CheckType;
use Adranetwork\HealthChecker\Exceptions\HealthCheckFailedException;
use Illuminate\Support\Facades\Log;
use TiMacDonald\Log\LogEntry;
use TiMacDonald\Log\LogFake;

use function Pest\Laravel\getJson;

it('has 200 status', function () {
    config([
        'health-checker.checks' => [
            [
                'enabled' => true,
                'type' => CheckType::Database,
            ],
        ],
    ]);
    config()->set('health-checker.current_release_tag', $tag = 'some_release_tag');

    getJson(route('health-checker.health'))
        ->assertSuccessful()
        ->assertSee('passed')
        ->assertSee($tag);

});

it('returns_500_when_check_fails', function () {
    config()->set('health-checker.current_release_tag', $tag = 'some_release_tag');

    config()->set('database.default', 'invalid');
    config([
        'health-checker.checks' => [
            [
                'enabled' => true,
                'type' => CheckType::Database,
            ],
        ],
    ]);
    \Pest\Laravel\withoutExceptionHandling();
    $response = getJson(route('health-checker.health'))
        ->assertServerError()
        ->assertSee('failed')
        ->assertSee($tag);

})->throws(HealthCheckFailedException::class);

it('it_logs_failure', function () {
    LogFake::bind();
    config()->set('database.default', 'invalid');

    config([
        'health-checker.checks' => [
            [
                'enabled' => true,
                'type' => CheckType::Database,
            ],
        ],
    ]);
    getJson(route('health-checker.health'))
        ->assertServerError();
    Log::assertLogged(fn (LogEntry $log) => $log->level === 'critical'
        //        && $log->message === 'User logged in.'
        //        && $log->context === ['user_id' => 5]
    );

});
